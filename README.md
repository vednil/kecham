# Установка Linux Mint

[[_TOC_]]


## Установка системы

1. Загружаем образ _Linux Mint 21.1_ (редакцию _MATE_) с сайта https://linuxmint.com.
2. Записываем образ на USB-накопитель, например, с помощью _Rufus_ с сайта https://rufus.ie.
3. Запускаем компьютер с подготовленного USB-накопителя.
   - _Установка операционной системы в режиме UEFI производится с отключенным Secure Boot._
4. Подключаем компьютер к интернету.
5. Устанавливаем операционную систему с помощью значка на рабочем столе _Install Linux Mint_.
   - Выбираем русский язык.
   - Оставляем русскую раскладку.
   - Устанавливаем галочку _Установка мультимедиа кодеков_.
   - Выполняем разбивку диска:
      - создаем новую таблицу разделов;
      - если компьютер запущен в режиме UEFI, то создаем раздел _EFI_ объемом _128 МБ_;
      - создаем раздел _/_ объемом _122 880 МБ_, то есть _120 ГБ_;
      - создаем раздел _/home_ из всего свободного пространства на диске.
   - Выбираем часовой пояс _Moscow_.
6. Перезагружаем компьютер после завершения установки.


## Настройка системы

1. Настраиваем интерфейс:
```
curl -s https://gitlab.com/vednil/kecham/-/raw/main/desktop.sh | bash
```
2. Обновляем систему:
```
sudo apt-get update && \
sudo apt-get -y dist-upgrade
```
3. Включаем автоматическое обновление системы и удаление старых ядр и зависимостей:
```
sudo mintupdate-automation upgrade enable && \
sudo mintupdate-automation autoremove enable
```
4. Настраиваем использование swap:
```
echo "vm.swappiness = 5" | sudo tee /etc/sysctl.d/99-swappiness.conf && \
sudo sysctl -p /etc/sysctl.d/99-swappiness.conf
```
5. Устанавливаем Chromium:
```
sudo apt-get install -y chromium
```
6. Меняем настройки по-умолчанию для Chromium:
```
sudo curl -s https://gitlab.com/vednil/kecham/-/raw/main/master_preferences -o /etc/chromium/master_preferences
```
7. Добавляем в Chromium:
   - блокировщик рекламы _uBlock Origin_;
   - поисковую систему Google (с помощью _URL_ https://google.com/search?q=%s).
8. Устанавливаем numlockx:
```
sudo apt-get install -y numlockx
```
9. Включаем numlock в окне входа в систему:
```
cat <<EOF | sudo tee /etc/lightdm/slick-greeter.conf
[Greeter]
activate-numlock=true
EOF
```


## Дополнительные действия

_Инструкции данного раздела необязательны и выполняются для включения поддержки некоторого оборудования._

- Устанавливаем драйвер видеокарт Nvidia:
```
sudo apt-get install -y nvidia-driver-525
```
```
cat <<EOF | sudo tee /etc/modules-load.d/nvidia.conf
nvidia
nvidia-drm
nvidia-modeset
EOF
```
- Устанавливаем драйвер сетевых карт Broadcom для включения поддержки Wi-Fi:
```
sudo apt-get install -y bcmwl-kernel-source
```
