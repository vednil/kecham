#!/bin/bash

echo "Добавляем ярлыки компьютера, домашней папки и корзины..."
gsettings set org.mate.caja.desktop computer-icon-visible true
gsettings set org.mate.caja.desktop home-icon-visible true
gsettings set org.mate.caja.desktop trash-icon-visible true

echo "Настраиваем хранитель экрана..."
gsettings set org.mate.screensaver lock-enabled false
gsettings set org.mate.screensaver idle-activation-enabled false
gsettings set org.mate.screensaver mode 'blank-only'
gsettings set org.mate.screensaver themes "@as []"
dconf write /org/mate/desktop/session/idle-delay 30

echo "Отключаем автозагрузку экрана приветствия..."
mkdir -p ~/.linuxmint/mintwelcome
touch ~/.linuxmint/mintwelcome/norun.flag

echo "Настраиваем клавиатуру..."
gsettings set org.mate.peripherals-keyboard-xkb.general group-per-window false
gsettings set org.mate.peripherals-keyboard-xkb.kbd options "['terminate\tterminate:ctrl_alt_bksp', 'grp\tgrp:alt_shift_toggle']"
gsettings set org.mate.peripherals-keyboard-xkb.kbd layouts "['us', 'ru']"

echo "Отключаем команду удаления из контекстного меню, не использующую корзину..."
gsettings set org.mate.caja.preferences enable-delete false

echo "Настраиваем предварительный просмотр файлов..."
gsettings set org.mate.caja.preferences show-image-thumbnails 'local-only'

echo "Копируем шаблоны документов..."
curl -s https://gitlab.com/vednil/kecham/-/raw/main/templates/libreoffice_calc.ods -o ~/Шаблоны/LibreOffice\ Calc.ods
curl -s https://gitlab.com/vednil/kecham/-/raw/main/templates/libreoffice_impress.odp -o ~/Шаблоны/LibreOffice\ Impress.odp
curl -s https://gitlab.com/vednil/kecham/-/raw/main/templates/libreoffice_writer.odt -o ~/Шаблоны/LibreOffice\ Writer.odt
curl -s https://gitlab.com/vednil/kecham/-/raw/main/templates/ms_excel.xlsx -o ~/Шаблоны/MS\ Excel.xlsx
curl -s https://gitlab.com/vednil/kecham/-/raw/main/templates/ms_word.docx -o ~/Шаблоны/MS\ Word.docx
